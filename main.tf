# unifi-test.management.bixbots.cloud

# https://community.ui.com/questions/UniFi-Installation-Scripts-or-UniFi-Easy-Update-Script-or-UniFi-Lets-Encrypt-or-Ubuntu-16-04-18-04-/ccbc7530-dd61-40a7-82ec-22b17f027776

# UniFi - Ports Used
# https://help.ui.com/hc/en-us/articles/218506997

# UniFi - Install a UniFi Cloud Controller on Amazon Web Services
# https://help.ui.com/hc/en-us/articles/209376117

# UniFi - Troubleshooting STUN Communication Errors
# https://help.ui.com/hc/en-us/articles/115015457668

# UniFi - Explaining the system.properties File
# https://help.ui.com/hc/en-us/articles/205202580

# UniFi - Device Adoption Methods for Remote UniFi Controllers
# https://help.ui.com/hc/en-us/articles/204909754

# UniFi - Advanced Adoption of a "Managed By Other" Device
# https://help.ui.com/hc/en-us/articles/205146020

locals {
  region      = "us-east-1"
  application = "unifi"
  environment = "management"
  role        = "test"
  lifespan    = "temporary"

  #spot_price    = "0.0052"
  instance_type = "t3.small"

  subnet_type = "private"
  key_name    = "platform-management"

  tags = {}
}

data "aws_ami" "primary" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  # http://uec-images.ubuntu.com/locator/ec2/
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "tags_base" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v2"

  application = local.application
  environment = local.environment
  lifespan    = local.lifespan
  tags        = local.tags
}

module "launch_pad" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git?ref=v2"

  application = local.application
  environment = local.environment
  role        = local.role

  enable_admin_access = true
  allow_all_egress    = true

  tags = module.tags_base.tags
}

data "template_file" "configure_unifi" {
  template = file("${path.module}/configure-unifi.sh")
}

module "standalone_ec2" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2?ref=v3"

  name        = module.launch_pad.name
  vpc_id      = module.launch_pad.vpc_id
  subnet_type = local.subnet_type

  #spot_price    = local.spot_price
  instance_type = local.instance_type
  image_id      = data.aws_ami.primary.id
  key_name      = local.key_name

  security_group_ids      = module.launch_pad.security_group_ids
  iam_instance_profile_id = module.launch_pad.iam_instance_profile_id

  rendered_cloud_init = [{
    filename     = "configure-unifi.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.configure_unifi.rendered
  }]

  tags = module.launch_pad.tags
}

data "aws_subnet_ids" "public" {
  vpc_id = module.launch_pad.vpc_id

  tags = {
    "Type" = "public"
  }
}

resource "aws_lb" "primary" {
  name    = module.launch_pad.name
  subnets = data.aws_subnet_ids.public.ids

  internal                         = false
  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true
  load_balancer_type               = "network"

  tags = module.launch_pad.tags
}

# DNS

resource "aws_route53_record" "primary" {
  zone_id = module.launch_pad.zone_id
  name    = "${local.application}-${local.role}"
  type    = "A"

  alias {
    name                   = aws_lb.primary.dns_name
    zone_id                = aws_lb.primary.zone_id
    evaluate_target_health = true
  }
}

# Certificate

resource "aws_acm_certificate" "primary" {
  domain_name       = aws_route53_record.primary.fqdn
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(module.launch_pad.tags, {
    "Name" = aws_route53_record.primary.fqdn
  })
}

resource "aws_route53_record" "validation" {
  zone_id         = module.launch_pad.zone_id
  allow_overwrite = true
  ttl             = 60

  # https://github.com/terraform-providers/terraform-provider-aws/issues/14447
  name    = aws_acm_certificate.primary.domain_validation_options.*.resource_record_name[0]
  records = [aws_acm_certificate.primary.domain_validation_options.*.resource_record_value[0]]
  type    = aws_acm_certificate.primary.domain_validation_options.*.resource_record_type[0]
}

resource "aws_acm_certificate_validation" "primary" {
  certificate_arn         = aws_acm_certificate.primary.arn
  validation_record_fqdns = [aws_route53_record.validation.fqdn]
}

# SSH (22)

resource "aws_lb_target_group" "port22" {
  name   = "${module.launch_pad.name}-port22"
  vpc_id = module.launch_pad.vpc_id

  port     = 22
  protocol = "TCP"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port22" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 22
  protocol = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port22.arn
  }
}

resource "aws_autoscaling_attachment" "port22" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port22.arn
}

resource "aws_security_group_rule" "allow_22_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# STUN (3478)

resource "aws_lb_target_group" "port3478" {
  name   = "${module.launch_pad.name}-port3478"
  vpc_id = module.launch_pad.vpc_id

  port     = 3478
  protocol = "UDP"

  health_check {
    port     = 8080
    protocol = "TCP"
  }

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port3478" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 3478
  protocol = "UDP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port3478.arn
  }
}

resource "aws_autoscaling_attachment" "port3478" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port3478.arn
}

resource "aws_security_group_rule" "allow_3478_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 3478
  to_port   = 3478
  protocol  = "UDP"

  cidr_blocks = ["0.0.0.0/0"]
}

# Mobile Speed Test (6789)

resource "aws_lb_target_group" "port6789" {
  name   = "${module.launch_pad.name}-port6789"
  vpc_id = module.launch_pad.vpc_id

  port     = 6789
  protocol = "TCP"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port6789" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 6789
  protocol = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port6789.arn
  }
}

resource "aws_autoscaling_attachment" "port6789" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port6789.arn
}

resource "aws_security_group_rule" "allow_6789_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 6789
  to_port   = 6789
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# HTTP (8080)

resource "aws_lb_target_group" "port8080" {
  name   = "${module.launch_pad.name}-port8080"
  vpc_id = module.launch_pad.vpc_id

  port     = 8080
  protocol = "TCP"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port8080" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 8080
  protocol = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port8080.arn
  }
}

resource "aws_autoscaling_attachment" "port8080" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port8080.arn
}

resource "aws_security_group_rule" "allow_8080_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 8080
  to_port   = 8080
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# HTTPS (8443)

resource "aws_lb_target_group" "port8443" {
  name   = "${module.launch_pad.name}-port8443"
  vpc_id = module.launch_pad.vpc_id

  port     = 8443
  protocol = "TLS"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port8443" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 8443
  protocol = "TLS"

  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = aws_acm_certificate.primary.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port8443.arn
  }
}

resource "aws_autoscaling_attachment" "port8443" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port8443.arn
}

resource "aws_security_group_rule" "allow_8443_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 8443
  to_port   = 8443
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# HTTPS Redirection (8843)

resource "aws_lb_target_group" "port8843" {
  name   = "${module.launch_pad.name}-port8843"
  vpc_id = module.launch_pad.vpc_id

  port     = 8843
  protocol = "TLS"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port8843" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 8843
  protocol = "TLS"

  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = aws_acm_certificate.primary.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port8843.arn
  }
}

resource "aws_autoscaling_attachment" "port8843" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port8843.arn
}

resource "aws_security_group_rule" "allow_8843_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 8843
  to_port   = 8843
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# HTTP Redirection (8880)

resource "aws_lb_target_group" "port8880" {
  name   = "${module.launch_pad.name}-port8880"
  vpc_id = module.launch_pad.vpc_id

  port     = 8880
  protocol = "TCP"

  tags = module.launch_pad.tags
}

resource "aws_lb_listener" "port8880" {
  load_balancer_arn = aws_lb.primary.arn

  port     = 8880
  protocol = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.port8880.arn
  }
}

resource "aws_autoscaling_attachment" "port8880" {
  autoscaling_group_name = module.standalone_ec2.autoscaling_group_name
  alb_target_group_arn   = aws_lb_target_group.port8880.arn
}

resource "aws_security_group_rule" "allow_8880_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 8880
  to_port   = 8880
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}
