provider "aws" {
  region  = "us-east-1"
  version = "~> 3.10.0"
}

terraform {
  required_version = ">= 0.13.4"

  backend "s3" {
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    bucket         = "bixbots-terraform-state"
    key            = "test/management/unifi"
    encrypt        = "true"
  }
}
