#!/usr/bin/env bash

# https://medium.com/tenable-techblog/lessons-from-aws-nlb-timeouts-5028a8f65dda
echo 120 > /proc/sys/net/ipv4/tcp_keepalive_time
echo 60 > /proc/sys/net/ipv4/tcp_keepalive_intvl
echo 6 > /proc/sys/net/ipv4/tcp_keepalive_probes
echo "net.ipv4.tcp_keepalive_time = 120" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_intvl = 60" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_probes = 6" >> /etc/sysctl.conf
sysctl -p

# https://help.ui.com/hc/en-us/articles/205202580
# /var/lib/unifi/system.properties
# /usr/lib/unifi/data/system.properties

# TODO: /var/lib/unifi//backup/autobackup

apt-get update; apt-get install ca-certificates wget -y

rm unifi-latest.sh &> /dev/null; wget https://get.glennr.nl/unifi/install/install_latest/unifi-latest.sh && bash unifi-latest.sh --skip
